import { client } from '../src/connection';

// client is the main object we will use to interact w/ our database

test("Should create a connection ", async ()=> {
    
    const result = await client.query('select * from account'); // we need await in 
    // order to get the information from our database    
});

// Do not leave up otherwise it will fail when executing site
afterAll(async ()=>{
    await client.end(); // should close our connections once our all tests are over
})