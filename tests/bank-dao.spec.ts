import { Account, BankClient } from "../src/entities";
import { BankDAO } from "../src/daos/bank-doa";
import { ClientDaoPostgres } from "../src/daos/client-dao-postgres";
import { client } from "../src/connection";

const bankDAO:BankDAO = new ClientDaoPostgres();

const createSSN = function (){
    const ssn:number = Math.floor(100000 + Math.random() * 900000)
    return ssn
}

test("Create our First Client  ", async () => {
    const testClient:BankClient = new BankClient(0, "Adam", "Ranieri", createSSN(), '1901/08/20');
    const bankClient:BankClient = await bankDAO.clientCreator(testClient);
    expect(bankClient.clientID).not.toBe(0);
});

test("Create our First Account  ", async () => {
    const testAccount:Account = new Account(1, 0, 'checking', 1000, 'open');
    const account:Account = await bankDAO.accountCreator(testAccount);
    expect(account.accountID).not.toBe(0);
});


test("Obtain a list of all clients:  ", async ()=>{
    const testClient2:BankClient = new BankClient(0, "Donny", "Merrin", createSSN(), '1999/06/22');
    const testClient3:BankClient = new BankClient(0, "Sandra", "Musz",createSSN(), '1981/04/05');
    const testClient4:BankClient = new BankClient(0, "Ash", "Naik", createSSN(), '1995/12/25');
    
    const testAccount1:Account = new Account(1, 1, 'saving', 100000, 'open');
    const testAccount2:Account = new Account(0, 0,'retirement', 2000, 'open');
    const testAccount3:Account = new Account(0, 0, 'saving', 3000, 'open');
    const testAccount4:Account = new Account(0, 0, 'checking', 5000000, 'frozen');
    const testAccount5:Account = new Account(4, 5, 'checking', 5000000, 'frozen');
    
    const client2 = await bankDAO.clientCreator(testClient2);
    const client3 = await bankDAO.clientCreator(testClient3);
    const client4 = await bankDAO.clientCreator(testClient4);
    
    testAccount2.clientID = client2.clientID;
    testAccount3.clientID = client3.clientID;
    testAccount4.clientID = client4.clientID;

    await bankDAO.accountCreator(testAccount1);
    await bankDAO.accountCreator(testAccount2);
    await bankDAO.accountCreator(testAccount3);
    await bankDAO.accountCreator(testAccount4);
    await bankDAO.accountCreator(testAccount5);
    const bankClient:BankClient[] = await bankDAO.allClients();

    expect(bankClient.length).toBeGreaterThanOrEqual(4);

    });

test("Check for account by accountID", async () => {
    const account:Account = await bankDAO.accountByAccountID(4);
    expect(account.accountID).toBe(4)

})
test("Check for client by clientID", async () => {
    const bankClient:BankClient = await bankDAO.clientByID(4);
    expect(bankClient.clientID).toBe(4)

})
test("Check for accounts by clientID", async () => {
    const account:Account[] = await bankDAO.accountsByClientID(1);
    expect(account.length).toBeGreaterThanOrEqual(2)

})
test("Updating the account  ", async ()=>{
    const testAccount:Account = new Account(2, 3, 'retirement', 4000, 'open');
    const account:Account = await bankDAO.accountUpdate(testAccount);
    expect(account.amount).toBe(4000);
})


test("Updating the client  ", async ()=>{
    const testClient:BankClient = new BankClient(3, 'Chuck', 'Norris', createSSN(), '1900/01/01');
    const bankClient:BankClient = await bankDAO.clientUpdate(testClient);
    expect(bankClient.ssn).toBe(testClient.ssn);
})

test("Modify the amount for withdrawal:  ", async()=>{
    const testAccount = new Account(4,6,'checking',5000000,'frozen')
    const result = await bankDAO.modifyAmount(testAccount, 500, 1)
    expect(result.amount).toBe(4999500)
})

test("Modify the amount for desposit: ", async ()=>{
    const testAccount = new Account(4,6,'checking',4999500,'frozen')
    const result = await bankDAO.modifyAmount(testAccount, 500, 0)
    expect(result.amount).toBe(5000000)
})

test("Delete the following account ", async ()=>{
    const testClient:BankClient = new BankClient(0, "Vanessa", "Hogan", createSSN(), '1901/08/20');
    const bankClient:BankClient = await bankDAO.clientCreator(testClient);
    const testAccount:Account = new Account(bankClient.clientID, 0, 'saving', 100000, 'open');
    const testClient2:BankClient = new BankClient(0, "Hulk", "Hogan", createSSN(), '1901/08/20');
    const bankClient2:BankClient = await bankDAO.clientCreator(testClient2);
    const testAccount2:Account = new Account(bankClient2.clientID, 0, 'saving', 100000, 'open');
    const account2:Account =await bankDAO.accountCreator(testAccount)
    const account:Account = await bankDAO.accountCreator(testAccount2)


    const result:boolean = await bankDAO.accountDeletion(account2.accountID);
    expect(result).toBeTruthy();
})
test("Check for accounts by range ", async () => {
    const accounts:Account[] = await bankDAO.accountsRange(1000, 5000)

    expect(accounts.length).toBeGreaterThanOrEqual(3)
})

test("Delete the following client ", async ()=>{
    const testClient1:BankClient = new BankClient(0, "P", "Diddy", createSSN(), '1999/06/22');
    const testAccount1:Account = new Account(0, 0,'retirement', 200000000, 'open');
    const testAccount2:Account = new Account(0, 0,'checking', 200000000, 'open');
    
    const testClient2:BankClient = new BankClient(0, "Blah", "Blah", createSSN(), '1999/06/22');
    const testAccount3:Account = new Account(0, 0,'retirement', 200000000, 'open');
    const testAccount4:Account = new Account(0, 0,'checking', 200000000, 'open');

    const clientResult = await bankDAO.clientCreator(testClient1);
    testAccount1.clientID = clientResult.clientID
    testAccount2.clientID = clientResult.clientID
    await bankDAO.accountCreator(testAccount1);
    await bankDAO.accountCreator(testAccount2);

    const clientResult1 = await bankDAO.clientCreator(testClient2);
    testAccount3.clientID = clientResult.clientID
    testAccount4.clientID = clientResult.clientID
    await bankDAO.accountCreator(testAccount3);
    await bankDAO.accountCreator(testAccount4);
    
    
    const result:boolean = await bankDAO.clientDeletion(clientResult1.clientID);
    
    expect(result).toBeTruthy();
})

afterAll(async ()=>{
    await client.end(); // should close our connections once our all tests are over
  })