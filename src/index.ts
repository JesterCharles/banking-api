import express from 'express';
import { Account, BankClient } from './entities';
import BankServices from './services/bank-services';
import { BankServicesImpl } from './services/bank-servicces-impl';
import { ClientExists, ConflictingIdentifications, InsufficientFunds, MissingResourceError } from './errors';

const app = express();
app.use(express.json());

const bankServices:BankServices = new BankServicesImpl()
// Here begins our read requests
app.get("/clients/:id", async (req,res) => {
    try {
        const clientID:number = Number(req.params.id)
        const result:BankClient = await bankServices.clientByID(clientID);
        res.send(result)
    } catch(error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error)
        }
    }
})

app.get("/clients", async (req,res) => {
    const result:BankClient[] = await bankServices.allClients();
    res.status(200)
    res.send(result);
 
})

app.get("/clients/:id/accounts", async (req,res)=>{
    try {
        const clientID = Number(req.params.id)
        const accounts:Account[] = await bankServices.accountsByID(clientID)
        res.send(accounts)
    } catch(error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error)
        }
    }
})

app.get("/accounts/:id", async(req,res)=>{
    try {
        const accountID = Number(req.params.id);
        const account:Account = await bankServices.accountByID(accountID);
        res.send(account);
    } catch(error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error)
        }
    }
})

app.get('/accounts', async (req,res)=>{
    const max:number = Number(req.query.amountLessThan) ;
    const min:number = Number(req.query.amountGreaterThan) ;
    const accounts:Account[] = await bankServices.accountRange(min, max);
    res.send(accounts)
})

// Here is were we start to post things
app.post("/clients", async(req,res) => {
    try{ 
        const bankClient:BankClient = req.body
        const result:BankClient = await bankServices.registerClient(bankClient)
        res.status(201)
        res.send(result)
    } catch(error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error)
        }
        if (error instanceof ClientExists){
            res.status(409)
            res.send(error)
        }
    }
})

app.post("/clients/:id/accounts", async(req,res) => {
    try{
        const account:Account = req.body
        const clientID:number = Number(req.params.id)
        const result:Account = await bankServices.registerAccount(account,'checking',clientID)
        res.status(201)
        res.send(result)
    } catch(error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error)
        }
        if (error instanceof ConflictingIdentifications){
            res.status(409);
            res.send(error)
        }
    }
})

// Here we beging the PUT   
app.put("/accounts/:id", async (req, res) => {
    try {
        const accountID:number = Number(req.params.id);
        const account:Account = req.body;
        const moddedAccount:Account = await bankServices.accountUpdate(accountID,account)
        res.send(moddedAccount)
    } catch(error) {
        if (error instanceof ConflictingIdentifications){
            res.status(409);
            res.send(error)
        }
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error)
        }
    }
})

app.put("/clients/:id", async (req,res) => {
    try{
        const clientID = Number(req.params.id)
        const bankClient:BankClient = req.body;
        const moddedClient:BankClient = await bankServices.clientUpdate(clientID, bankClient);
        res.send(moddedClient)
    } catch(error) {
        if (error instanceof ConflictingIdentifications){
            res.status(409);
            res.send(error)
        }
        if (error instanceof MissingResourceError){
            res.status(409);
            res.send(error)
        }
    }
})

// Here we begin the PATCH
app.patch("/accounts/:id/deposit", async (req,res) => {
    try { const accountID:number = Number(req.params.id);
        const withdrawal:number = req.body.amount;
        const result:Account = await bankServices.accountDeposit(accountID, withdrawal)
        res.send(result)
    } catch(error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error)
        }
    }
})

app.patch("/accounts/:id/withdraw", async (req,res) => {
    try {
        const accountID:number = Number(req.params.id);
        const withdrawal:number = req.body.amount;
        const result:Account = await bankServices.accountWithdrawal(accountID, withdrawal)
        res.send(result)
    } catch(error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error)
        }
        if (error instanceof InsufficientFunds) {
            res.status(422)
            res.send(error)
        }
    }

})

// Here we begin executing...i mean DELETING clients and accounts :D
app.delete("/clients/:id", async (req,res) => {
    try {
        const clientID = Number(req.params.id)
        await bankServices.removeClient(clientID)
        res.status(205)
        res.send(`The Client with ID ${clientID} has been deleted.`)
    } catch(error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error)
        }
    }
});

app.delete("/accounts/:id", async (req,res) => {
    try {
        const accountID = Number(req.params.id)
        await bankServices.removeAccount(accountID)
        res.status(205);
        res.send(`The Account with ID ${accountID} has been deleted.`)
    } catch(error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error)
        }
    }
});


app.listen(3000, ()=>{console.log("Application Started")})