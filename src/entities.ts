
export class Account{
    constructor(
    public clientID:number,
    public accountID:number,
    public accountType:string,
    public amount:number,
    public accountStatus:string
    ){}
}

export class BankClient{
    constructor(
        public clientID:number,
        public fname:string,
        public lname:string,
        public ssn:number,
        public birthdate:string
    ){}
}