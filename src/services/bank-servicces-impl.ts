import { BankClient, Account } from "../entities";
import BankServices from "./bank-services";
import { BankDAO } from "../daos/bank-doa";
import { ClientDaoPostgres } from "../daos/client-dao-postgres";
import { ConflictingIdentifications, InsufficientFunds, MissingResourceError } from "../errors";

export class BankServicesImpl implements BankServices{
    bankDAO:BankDAO = new ClientDaoPostgres()

    registerClient(bankClient: BankClient): Promise<BankClient> {
        return this.bankDAO.clientCreator(bankClient);
    }

    async registerAccount(account:Account, accountType:string,clientID:number):Promise<Account> {
        const bankClient:BankClient = await this.bankDAO.clientByID(clientID)
        if(account.clientID != clientID){
            throw new ConflictingIdentifications(`Requesting to add to client ${clientID} but client ${account.clientID} was provided. Insure correct client ID is present before proceeding.`)
        } else if (bankClient.clientID != clientID){
            throw new MissingResourceError(`The ${clientID} provided does not exist in out systems`)
        }
        account.accountType = accountType;
        return this.bankDAO.accountCreator(account)
    }

    allClients():Promise<BankClient[]>{
        return this.bankDAO.allClients();
    }

    async accountByID(accountID: number): Promise<Account> {
        const account:Account = await this.bankDAO.accountByAccountID(accountID)
        if (accountID != account.accountID){
            throw new MissingResourceError(`There is no account with the ID of ${accountID}`);
        }
        return this.bankDAO.accountByAccountID(accountID);
    }

    clientByID(clientID: number): Promise<BankClient> {
        return this.bankDAO.clientByID(clientID)
    }

    accountsByID(clientID:number):Promise<Account[]>{
        return this.bankDAO.accountsByClientID(clientID);
    }

    async accountBalance(accountID: number): Promise<number> {
        const account:Account = await this.bankDAO.accountByAccountID(accountID);
        return account.amount
    }
    
    async accountRange(minBalance: number, maxBalance: number): Promise<Account[]> {
        const accounts:Account[] = await this.bankDAO.accountsRange(minBalance, maxBalance);
        return accounts;
    }

    async clientUpdate(clientID:number,bankClient:BankClient):Promise<BankClient>{
        const testCID:BankClient = await this.bankDAO.clientByID(bankClient.clientID);
        if (bankClient.clientID != clientID){
           throw new ConflictingIdentifications(`There is no client with the ID of ${clientID}`);
        } else if (testCID.clientID != clientID) {
            throw new MissingResourceError(`There is no client with the ID of ${bankClient.clientID}`);
        }
        return this.bankDAO.clientUpdate(bankClient);
    }

    async accountUpdate(accountID:number, account: Account): Promise<Account> {
        if (account.accountID != accountID){
            throw new ConflictingIdentifications(`There is no client with the ID of ${accountID}`);
         }
        const testAID:Account = await this.bankDAO.accountByAccountID(accountID);
        const testCID:BankClient = await this.bankDAO.clientByID(account.clientID);
        
        if (testAID.accountID != accountID){
            throw new MissingResourceError(`There is no account with the ID of ${accountID}`);
         }
        if (testCID.clientID != account.clientID){
            throw new MissingResourceError(`There is no client with the ID of ${account.clientID}`);
        } else {
            await this.bankDAO.accountUpdate(account)
        }
    
        return this.bankDAO.accountUpdate(account)
    }

    async accountWithdrawal(accountID: number, amount: number): Promise<Account> {
        const account:Account = await this.bankDAO.accountByAccountID(accountID)
        if(account.accountID != accountID){
            throw new MissingResourceError(`The account with ID ${accountID} does not exist in our systems`);
        }
        if (account.amount < amount) {
            throw new InsufficientFunds(`The current amount in your account is ${account.amount}`);
        }
        ;
        await this.bankDAO.modifyAmount(account, amount, 1)
        return account;
    }
    async accountDeposit(accountID: number, amount: number): Promise<Account> {
        const account:Account = await this.bankDAO.accountByAccountID(accountID)
        if(account.accountID != accountID){
            throw new MissingResourceError(`The account with ID ${accountID} does not exist in our systems`);
        }
        await this.bankDAO.modifyAmount(account, amount, 0)

        return account;
    }
    async removeClient(clientID:number): Promise<boolean> {
        const bankClient:BankClient = await this.bankDAO.clientByID(clientID)
        if (bankClient.clientID != clientID){
            throw new MissingResourceError(`There is no client with the ID of ${clientID}`);
        }
        return this.bankDAO.clientDeletion(clientID)
    }
    async removeAccount(accountID:number): Promise<boolean> {
        const account:Account = await this.bankDAO.accountByAccountID(accountID)
        if (accountID != account.accountID){
            throw new MissingResourceError(`There is no account with the ID of ${accountID}`);
        }
        return this.bankDAO.accountDeletion(accountID)
    }

}