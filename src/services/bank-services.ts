import { Account, BankClient } from "../entities";


export default interface BankServices{

    registerClient(bankClient:BankClient):Promise<BankClient>;
    registerAccount(account:Account, accountType:string,id:number):Promise<Account>;

    allClients():Promise<BankClient[]>;
    clientByID(clientID:number):Promise<BankClient>;
    accountByID(accountID:number):Promise<Account>;
    accountsByID(clientID:number):Promise<Account[]>
 
    accountBalance(accountID:number):Promise<number>;
    accountRange(minBalance:number, maxBalance:number):Promise<Account[]>;

    accountUpdate(accountID:number, account:Account):Promise<Account>;
    clientUpdate(clientID:number,bankClient:BankClient):Promise<BankClient>;
    accountWithdrawal(accountID: number, amount:number): Promise<Account>;
    accountDeposit(accountID:number, amount:number):Promise<Account>;

    removeClient(clientID:number):Promise<boolean>;
    removeAccount(accountID:number):Promise<boolean>
}