
import { Account, BankClient } from "../entities";

export interface BankDAO {
    // CREATE
    clientCreator(bankClient:BankClient):Promise<BankClient>;
    accountCreator(account:Account):Promise<Account>;

    // Read
    accountsRange(minBalance:number, maxBalance:number):Promise<Account[]>;
    allClients():Promise<BankClient[]>;
    accountByAccountID(accountID: number):Promise<Account>;
    accountsByClientID(clientID:number):Promise<Account[]>;
    clientByID(clientID:number):Promise<BankClient>;

    // UPDATE
    accountUpdate(account:Account):Promise<Account>;
    clientUpdate(bankClient:BankClient):Promise<BankClient>;
    modifyAmount(account:Account, amount:number, modifyType:number):Promise<Account>;

    // Delete
    accountDeletion(accountID:number):Promise<boolean>;
    clientDeletion(clientID: number): Promise<boolean>

}