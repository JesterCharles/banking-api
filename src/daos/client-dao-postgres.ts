import { BankDAO } from "./bank-doa";
import { Account, BankClient } from "../entities";
import { Client } from "pg";
import { client } from "../connection";
import { textChangeRangeIsUnchanged, textSpanContainsPosition } from "typescript";
import { ClientExists, MissingResourceError } from "../errors";



export class ClientDaoPostgres implements BankDAO{
 
    async clientCreator(bankClient:BankClient):Promise<BankClient>{
        const sqlTest:string = 'select * from bankclient where ssn = $1'
        const valueTest = [bankClient.ssn]
        const resTest = await client.query(sqlTest,valueTest)
        if( resTest.rowCount === 0){
            const sql:string = "insert into bankclient(fname,lname,ssn,birthday) values ($1,$2,$3,$4) returning client_id";
            const values = [bankClient.fname,bankClient.lname,bankClient.ssn,bankClient.birthdate];
            const result = await client.query(sql,values);
            bankClient.clientID = result.rows[0].client_id;
            return bankClient;
        } else { 
            throw new ClientExists(`This client already exist within the database: ${bankClient.fname} and ${bankClient.lname} with a ssn that is ${bankClient.ssn}`)
        }
    }
    async accountCreator(account:Account): Promise<Account> {
        const sql:string = "insert into account(account_type,amount,account_status,client_id) values ($1,$2,$3,$4) returning account_id";
        const values = [account.accountType,account.amount,account.accountStatus,account.clientID];
        const result = await client.query(sql, values);
        account.accountID = result.rows[0].account_id;
        
        return account;
    }

    async accountsRange(minBalance:number, maxBalance:number): Promise<Account[]> {
        const sql:string = "select * from account where amount between $1 and $2 order by account_id asc";
        const values = [minBalance, maxBalance]
        const result = await client.query(sql, values);
        const accounts:Account[] = [];
        for(const row of result.rows){
            const account:Account = new Account(
                row.client_id,
                row.account_id,
                row.account_type,
                row.amount,
                row.account_status
            );
            accounts.push(account);
        }

        return accounts;
    }

    async allClients(): Promise<BankClient[]> {
        const sql:string = "select * from bankclient order by client_id asc";
        const result = await client.query(sql);
        const clients:BankClient[] = [];
        for(const row of result.rows){
            const client:BankClient = new BankClient(
                row.client_id,
                row.fname,
                row.lname,
                row.ssn,
                row.birthday
            );
            clients.push(client);
        }

        return clients;
    }

    async accountByAccountID(accountID: number): Promise<Account> {
             
        const sql:string = "select * from account where account_id = $1";
        const value = [accountID]
        const result = await client.query(sql, value);
        if(result.rowCount === 0){
            throw new MissingResourceError(`There is no Account with the ID of ${accountID}`);
        }
        const row = result.rows[0]
        const account:Account = new Account(
            row.client_id,
            row.account_id,
            row.account_type,
            row.amount,
            row.account_status
        )
        return account;
    }

    async clientByID(clientID: number): Promise<BankClient> {
        const sql:string = "select * from bankclient where client_id = $1";
        const value = [clientID]
        const result = await client.query(sql, value);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The client with ID ${clientID} does not exist in our systems`);
        }
        const row = result.rows[0]
        const bankClient:BankClient = new BankClient(
            row.client_id,
            row.fname,
            row.lname,
            row.ssn,
            row.birthday
        );
        return bankClient;
    }
    
    async accountsByClientID(clientID:number): Promise<Account[]> {
        const sql:string = "select * from account where client_id = $1";
        const value = [clientID]
        const result = await client.query(sql, value);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The client with ID ${clientID} does not exist in our systems`);
        }
        const accounts:Account[] = [];
        for(const row of result.rows){
            const account:Account = new Account(
                row.client_id,
                row.account_id,
                row.account_type,
                row.amount,
                row.account_status
            );
            accounts.push(account);
        }
        return accounts;
    }
     

    async accountUpdate(account: Account): Promise<Account> {
        const sql:string = 'update account set account_type=$1, amount=$2, account_status=$3 where account_id=$4 and client_id=$5';
        const values = [account.accountType,account.amount,account.accountStatus,account.accountID, account.clientID];
        const result = await client.query(sql,values);
        return account;
    }

    async clientUpdate(bankClient:BankClient):Promise<BankClient>{
        const sql:string = 'update bankclient set fname=$1, lname=$2, ssn=$3, birthday=$4 where client_id=$5    ';
        const values = [bankClient.fname,bankClient.lname,bankClient.ssn,bankClient.birthdate,bankClient.clientID];
        const result = await client.query(sql,values);
        return bankClient;
    }

    async modifyAmount(account:Account, amount:number, modifyType:number):Promise<Account>{
        if(modifyType === 0){
        const sql:string = 'update account set amount = amount + $1 where account_id = $2 returning amount'
        const value = [amount, account.accountID];
        const result = await client.query(sql, value);
        account.amount = result.rows[0].amount
        } else {
            const sql:string = 'update account set amount = amount - $1 where account_id = $2 returning amount'
        const value = [amount, account.accountID];
        const result = await client.query(sql, value);
        account.amount = result.rows[0].amount
        }
        

        return account;
    }

    async accountDeletion(accountID: number): Promise<boolean> {
        const sql:string = 'delete from account where account_id = $1';
        const value = [accountID];
        const result = await client.query(sql, value);

        return true;
    }

    async clientDeletion(clientID: number): Promise<boolean> {
        const sql:string = 'delete from account where client_id = $1';
        const value = [clientID];
        const result = await client.query(sql, value);
        
        const sql2:string = 'delete from bankclient where client_id = $1';
        const value2 = [clientID];
        await client.query(sql2, value2);

        return true;
    }
    
}

