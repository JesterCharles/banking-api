import {Client} from 'pg';
require('dotenv').config({path:'D:\\Revature\\defaultCodeRepository\\project-0\\app.env'});

export const client = new Client({
    user:'postgres',
    password:process.env.DBPASSLIB, // YOU SHOULD NEVER STORE PASSWORDS IN CODE
    database:process.env.DBNAMELIB,
    port:5432,
    host:'34.138.119.169'
})
client.connect()