

export class MissingResourceError{

    message:string;
    description:string = 'This error means a resource could not be located.';
    
    constructor(message:string){
        this.message = message;
    }
}

export class ConflictingIdentifications{
    message:string;
    description:string = 'This error means there is a confliction with the ID entered'
    
    constructor(message:string){
        this.message = message;
    }
}

export class InsufficientFunds{
    message:string;
    description:string = 'There is a lack of funds to be withdrawn from the account'
    
    constructor(message:string){
        this.message = message;
    }
}

export class ClientExists{
    message:string;
    description:string = 'This client already exists'
    
    constructor(message:string){
        this.message = message;
    }
}