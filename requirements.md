# Required Endpoints
## GETS
    - /clients              => return all clients
    - /client/##            => get clientID with specified number
    - /clients/##/accounts  => get all accounts for clientID w/ specified number
    - /accounts/#           => get account w/ accountID
    
## POSTS
    - /clients              => return all clients
    - /client/##            => get clientID with specified number



# QUESTIONS
- Ask about PUT w/ Endpoints
- Ask about returning inputs